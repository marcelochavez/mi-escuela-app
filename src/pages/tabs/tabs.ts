import { Component } from '@angular/core';
import {EventosPage} from "../eventos/eventos";
import {CursosPage} from "../cursos/cursos";
import {PerfilPage} from "../perfil/perfil";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = EventosPage;
  tab2Root = CursosPage;
  tab3Root = PerfilPage;

  constructor() {

  }
}
