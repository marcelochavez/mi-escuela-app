import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {EventProvider} from "../../providers/event/event";
import {EventModel} from "../../models/event";
import {GlobalEventModel} from "../../models/GlobalEventModel";
import {CourseInstanceEventModel} from "../../models/CourseInstanceEventModel";
import {dataUserModel} from "../../models/dataUser";
import {DetalleEventoPage} from "../detalle-evento/detalle-evento";

/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html',
})
export class EventosPage {
    public allEvents: EventModel[] = [];
    public globalEvents: GlobalEventModel[] = [];
    public courseInstanceEvents: CourseInstanceEventModel[] = [];
    public userEvents: dataUserModel[] = [];
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _providerEvent: EventProvider,
              public loadingCtrl: LoadingController) {
  }

  ionViewLoaded() {
      let loading = this.loadingCtrl.create({
          content: 'Cargando Eventos'
      });

      loading.present();

      setTimeout(() => {
          loading.dismiss();
      }, 900);
  }

  ionViewWillEnter(){
      this.ionViewLoaded();
      this.cargarAllEvent();
      this.cargarGlobalEvent();
      this.cargarCourseInstanceEvent();
      this.cargarUserEvent();
  }
  ionViewDidLoad() {
      console.log('ionViewDidLoad EventosPage');
  }
    cargarGlobalEvent() {
        this._providerEvent.getGlobalEvent().then(
            value => {
                this.globalEvents = value['data'];
                console.log(value);
            }, error => {
            }
        );
    }
    cargarCourseInstanceEvent() {
        this._providerEvent.getCourseInstanceEvent().then(
            value => {
                this.courseInstanceEvents = value['data'];
                console.log(value);
            }, error => {
            }
        );
    }
    cargarUserEvent() {
        this._providerEvent.getUserEvent().then(
            value => {
                this.userEvents = value['data'];
                console.log(value);
            }, error => {
            }
        );
    }
    cargarAllEvent() {
        this._providerEvent.getAllEvent().then(
            value => {
                this.allEvents = value['data'];
                console.log(value);
            }, error => {
            }
        );
    }
    irDetalleEvento(event){
      this.navCtrl.push(DetalleEventoPage, {event: event});
    }

    doRefresh(refresher) {
        this.cargarAllEvent();
        this.cargarGlobalEvent();
        this.cargarCourseInstanceEvent();
        this.cargarUserEvent();
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 700);
    }



}
