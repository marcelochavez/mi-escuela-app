import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {LoginPage} from "../login/login";

/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  email;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _providerUser: UserProvider,
              public alerta: AlertController,
              public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

    ionViewLoaded() {
        let loading = this.loadingCtrl.create({
            content: 'Actualizando Contraseña'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 1000);
    }


    restaurarPass(email){
    const param = {
      'email': email
    };
    console.log(param);
      this._providerUser.resetPassword(param).then(
          res => {
              let alert = this.alerta.create({
                  title: 'Restauración de Contraseña',
                  message: 'Se ha enviado un contraseña a su Correo Electrónico',
                  buttons: ['Aceptar']
              });
              alert.present();
              this.navCtrl.push(LoginPage);
          },
          error => {
              let errorAlert = this.alerta.create({
                  title: 'Error al restaurar Contraseña',
                  message: 'Favor, Verifique que el Correo sea el correcto',
                  buttons: ['Aceptar']
              });
              errorAlert.present();
          }
      );

  }
}
