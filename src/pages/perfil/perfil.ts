import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {dataUserModel} from "../../models/dataUser";
import {UserProvider} from "../../providers/user/user";
import {LoginPage} from "../login/login";

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  public userData: dataUserModel = new dataUserModel();
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _providerUser: UserProvider,
              public alerta: AlertController,
              public loadingCtrl: LoadingController) {
  }

  ionViewWillEnter(){
      this.ionViewLoaded();
        this.obtenerUsuario();
  }

  ionViewDidLoad() {
      this.ionViewLoaded();
      this.obtenerUsuario();
    console.log('ionViewDidLoad PerfilPage');
  }

    ionViewLoaded() {
        let loading = this.loadingCtrl.create({
            content: 'Cargando Perfil'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 1000);
    }



    obtenerUsuario() {
    this._providerUser.getDataUser().then(
      value => {
        this.userData = value['data'];
        console.log(this.userData);
      },
      error => {
        console.log(error);
      }
    );
  }
  editarPerfil(){
    this._providerUser.updateUser(this.userData).then(
        res => {
            let alert = this.alerta.create({
                title: 'Éxito',
                message: 'Se Registraron los Cambios de Forma Correcta',
                buttons: ['Aceptar']
            });
            alert.present();
        },
        error => {
            let errorAlert = this.alerta.create({
                title: 'Error al Editar',
                message: 'Favor, Verifique la información',
                buttons: ['Aceptar']
            });
            errorAlert.present();
        }
    );
    console.log(this.userData);
  }

  cerrarSesion(){
      this.ionViewLoadedCerrarSesion();
      this.navCtrl.setRoot(LoginPage);
      this._providerUser.logout();
      // this.navCtrl.push(LoginPage);
  }
    ionViewLoadedCerrarSesion() {
        let loading = this.loadingCtrl.create({
            content: 'Cerrando sesión'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 500);
    }

    doRefresh(refresher) {
        this.obtenerUsuario();
        this.obtenerUsuario();
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 500);
    }

}
