import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ChangePassModel} from "../../models/authentication";
import {UserProvider} from "../../providers/user/user";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the CambiarPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cambiar-password',
  templateUrl: 'cambiar-password.html',
})
export class CambiarPasswordPage {
  public newPass = new ChangePassModel();
  constructor(public navCtrl: NavController, public navParams: NavParams, public _providerUser: UserProvider,
              public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CambiarPasswordPage');
  }

  cambiarPass(){
      this._providerUser.changePassgord(this.newPass).then(
          value => {
              this.ionViewLoadedPassword();
              this.navCtrl.push(TabsPage);
       //       this.navCtrl.push(EventosPage);
          }, error => {
          }
      );

  }
    ionViewLoadedPassword() {
        let loading = this.loadingCtrl.create({
            content: 'Actualizando Contraseña'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 500);
    }

}
