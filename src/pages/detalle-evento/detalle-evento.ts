import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the DetalleEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-evento',
  templateUrl: 'detalle-evento.html',
})
export class DetalleEventoPage {
  event;
  description;
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController) {
      this.event = navParams.data.event;
      this.obtenerDescripcion();
      console.log(this.event);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleEventoPage');
  }


  obtenerDescripcion(){
      if (this.event.eventoPara == 'Evento Global'){
            this.description = this.event.global_events[0].description;
      }else if (this.event.eventoPara =='Evento de Estudiante'){
            this.description = this.event.users_has_events[0].description;
      }else if (this.event.eventoPara == 'Evento de Curso'){
          this.description = this.event.course_instance_has_events[0].description;
      }

  }

}
