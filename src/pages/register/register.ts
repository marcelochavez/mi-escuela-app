import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {UserRegisterModel} from "../../models/userRegister";
import {UserProvider} from "../../providers/user/user";
import {LoginPage} from "../login/login";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public userData = new UserRegisterModel();
  constructor(public navCtrl: NavController, public navParams: NavParams, public _providerUser: UserProvider,public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

    registerForm() {
    console.log(this.userData);
        this._providerUser.registerStudent(this.userData).then(
            value => {
                this.ionViewLoadedRegistrar();
                this.navCtrl.push(LoginPage);
            }, error => {
            }
        );
    }
    ionViewLoadedRegistrar() {
        let loading = this.loadingCtrl.create({
            content: 'Registrando Usuario'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 1000);
    }

}
