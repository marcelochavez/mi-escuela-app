import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import { AuthenticationModel } from "../../models/authentication";
import { LoginProvider} from "../../providers/login/login";
import {TabsPage} from "../tabs/tabs";
import {RegisterPage} from "../register/register";
import {CambiarPasswordPage} from "../cambiar-password/cambiar-password";
import {AlertController} from "ionic-angular";
import {ResetPasswordPage} from "../reset-password/reset-password";
import { FCM } from '@ionic-native/fcm';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public userAuth = new AuthenticationModel();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _loginService: LoginProvider,
              public alerta: AlertController,
              private fcm: FCM,
              public platform: Platform,
              public loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
      localStorage.clear();
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    this._loginService.loginUser(this.userAuth).then(
      (value) => {
        console.log(value['data'].token);
        window.localStorage.setItem('token', value['data'].token);
        if (value['data'].changePass){

            this.navCtrl.push(CambiarPasswordPage);


        }else{

            if (this.platform.is('android')){
                this.fcm.getToken().then(token => {
                    const params = {
                        'device': token,
                        'users_id':value['data'].id
                    };
                     console.log('PARAMETROS:'+params);
                    this._loginService.registerUserDevice(params).then(
                        value => {
                            //  console.log('PARAMETROS:'+params);
                        }, error => {
                            //  console.log("Error")
                        }
                    );


                    // Enviar usuario y token al back
                    // this._loginService.registerUserDevice(userId, token).then(_ => {
                    this.navCtrl.push(TabsPage);
                    //});

                });

            }else{
                this.navCtrl.push(TabsPage);
            }


        }
      },
      error => {
          let errorAlert = this.alerta.create({
              title: 'No se pudo Iniciar Sesión',
              message: 'Contraseña o Correo Incorrectos',
              buttons: ['Cerrar']
          });
          errorAlert.present();
        console.log(error);
      }
    )
  }
  irRegistrar(){
    this.navCtrl.push(RegisterPage);
  }
  restaurarPass(){
      this.navCtrl.push(ResetPasswordPage);
  }
}
