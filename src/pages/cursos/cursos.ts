import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {CourseProvider } from "../../providers/course/course";
import {CourseInstanceModel, CourseModel} from "../../models/course";
import {UserProvider} from "../../providers/user/user";
import {dataUserModel} from "../../models/dataUser";




/**
 * Generated class for the CursosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cursos',
  templateUrl: 'cursos.html',
})
export class CursosPage {
  public courses: CourseModel[] = [];
  public coursesStudent: CourseModel[] = [];
  public courseInstanceStudent: CourseInstanceModel[] = [];
  public allCourse: number[] = [];
  public cursosItem: any[] = [];
  public userData: dataUserModel = new dataUserModel();


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _providerCourse: CourseProvider,
              public _providerUser: UserProvider,
              public loadingCtrl: LoadingController) {

  }
    ionViewWillEnter(){
        this.ionViewLoaded();
        this.obtenerInstanciasCursosActuales();
        this.obtenerInstanciasCursosEstudiante();
        this.obtenerUsuario();
    }

  ionViewDidLoad() {
      this.obtenerInstanciasCursosActuales();
      this.obtenerInstanciasCursosEstudiante();
      this.obtenerUsuario();
    console.log('ionViewDidLoad CursosPage');
  }

    ionViewLoaded() {
        let loading = this.loadingCtrl.create({
            content: 'Cargando Cursos'
        });
        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 500);
    }


    obtenerInstanciasCursosActuales() {
    this._providerCourse.getListInstanceCourse().then(
      value => {
        this.courses = value['data'];
        console.log(this.courses);
      }, error => {
      }
    );
  }
    obtenerInstanciasCursosEstudiante(){
    this._providerCourse.getIntanceCourseByStudent().then(
        value => {
            this.courseInstanceStudent = value['data'];
            this.pasarACurso();
        }, error => {
        }
    );
    }
    pasarACurso(){
      let i;
      i = 0;
      while (i < this.courseInstanceStudent.length){
       this.coursesStudent[i] = this.courseInstanceStudent[i].course;
       //this.courseSelect[i] = this.courseInstanceStudent[i].course;
       this.allCourse[i] = this.courseInstanceStudent[i].course_id;
       i++;
      }
        this.pasarAItem();
    }


    pasarAItem(){
        let i,j, selected;
        i = 0;
        this.cursosItem = [{
            name: String('Seleccione Curso'), value: null, selected: false, disabled:true
        }];
        while (i < this.courses.length) {
          j = 0;
          selected = false;
          while (j < this.allCourse.length){
            if (this.courses[i].id == this.allCourse[j]){
              selected = true;
            }
            j++;
          }
          this.cursosItem.push({
              name: String(this.courses[i].code + ' ' + this.courses[i].name),
              value: this.courses[i].id,
              selected:selected,
              disabled: false});
          i++;
        }
        console.log(this.cursosItem);
    }
    actualizar(event, idCurso){

      if (event.checked){
          this.allCourse.push(idCurso)
          //agregar
      }else{
          let i;
          i = 0;
          while (i < this.allCourse.length){
              if (this.allCourse[i] == idCurso){
                  this.allCourse.splice(i, 1);
              }
              i++;
          }
      }
      console.log(this.allCourse);
    }

    guardar(){

        const paramStudent = {
            'name': this.userData.name,
            'last_name': this.userData.last_name,
            'email': this.userData.email,
            'phone': this.userData.phone,
            'course_id': this.allCourse
        };
        console.log(paramStudent);
        this._providerUser.studentUpdate(this.userData.id, paramStudent).then(
            res => {
                this.navCtrl.push(CursosPage);
            },
            error => {
                console.log(error);
            }
        );
    }
    obtenerUsuario() {
        this._providerUser.getDataUser().then(
            value => {
                this.userData = value['data'];
                console.log(this.userData);
            },
            error => {
                console.log(error);
            }
        );
    }
    doRefresh(refresher) {
        this.obtenerInstanciasCursosActuales();
        this.obtenerInstanciasCursosEstudiante();
        this.obtenerUsuario();
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 500);
    }
}

