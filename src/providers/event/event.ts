import {ApiConectionProvider} from "../api-conection/api-conection";
import { Injectable } from '@angular/core';

/*
  Generated class for the EventProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventProvider {

  constructor(private _apiConnection: ApiConectionProvider) {
    console.log('Hello EventProvider Provider');
  }
    getAllEvent() {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.get('getStudentEvents')
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                        console.log(res);
                    }, msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }


    getCourseInstanceEvent() {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.get('courseInstanceHasEvents')
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                    }, msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }
    getGlobalEvent() {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.get('globalEvents')
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                    }, msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }
    getUserEvent() {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.get('usersHasEvents')
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                    }, msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }

}
