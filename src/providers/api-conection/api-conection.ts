import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiConectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiConectionProvider {

  idToken: any;
  public url: any;
  constructor(public http: HttpClient) {
    //this.url = 'http://localhost:8000/api';
     // this.url = 'http://10.0.2.2:8000/api';
    //this.url = 'http://158.251.255.225:8000/api';
      this.url = 'http://192.168.0.15:8000/api';
     // this.url = 'http://10.8.227.2:8000/api';
  }

  createAuthorizationHeader(headers: HttpHeaders) {

    this.idToken = window.localStorage.getItem("token");
     console.log("the idtoken", this.idToken);
    headers = headers.set('Authorization', 'Bearer '+this.idToken);
    return headers;
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    let headers = new HttpHeaders({});
    headers =  this.createAuthorizationHeader(headers);
    // console.log(headers.get('Authorization'));
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams(),
        headers: headers
      }
    }
    // else{
    //   // console.log("entro else api");
    //   reqOpts = {
    //     headers: headers
    //   }
    // }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params.set(k, params[k]);
      }
    }
    return this.http.get(this.url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    let headers = new HttpHeaders({});
    headers =  this.createAuthorizationHeader(headers);
    reqOpts = {
      headers: headers
    };
    return this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    let headers = new HttpHeaders({});
    headers =  this.createAuthorizationHeader(headers);
    reqOpts = {
      headers: headers
    };
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    let headers = new HttpHeaders({});
    headers =  this.createAuthorizationHeader(headers);
    reqOpts = {
      headers: headers
    };
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    let headers = new HttpHeaders({});
    headers =  this.createAuthorizationHeader(headers);
    reqOpts = {
      headers: headers
    };
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }
}
