// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ApiConectionProvider} from "../api-conection/api-conection";

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {

  constructor(private _apiConnection: ApiConectionProvider) {
    console.log('Hello LoginProvider Provider');
  }

  loginUser(userInfo) {
      localStorage.clear();
    const promise = new Promise((resolve, reject) => {
      console.log(userInfo);
      this._apiConnection.post('auth/studentLogin', userInfo)
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }
  registerUserDevice(params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.post('usersHasDevices', params)
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                    },
                    msg =>{
                        reject(msg);
                    }
                );
        });
        return promise;
    }
}
