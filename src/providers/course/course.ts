
import { Injectable } from '@angular/core';
import {ApiConectionProvider} from "../api-conection/api-conection";

/*
  Generated class for the CourseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CourseProvider {

  constructor(private _apiConnection: ApiConectionProvider) {
    console.log('Hello CourseProvider Provider');
  }
  // Obtener cursos que perteneces a la instancia acutual
  getListInstanceCourse() {
    const promise = new Promise((resolve, reject) => {
      this._apiConnection.get('currentInstancesCourses')
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }
  getIntanceCourseByStudent() {
      const promise = new Promise((resolve, reject) => {
          this._apiConnection.get('getIntanceCourseByStudent')
              .toPromise()
              .then(
                  res => {
                      resolve(res);
                  },
                  msg => {
                      reject(msg);
                  }
              );
      });
      return promise;
  }
}
