import { Injectable } from '@angular/core';
import {ApiConectionProvider} from "../api-conection/api-conection";


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(private _apiConnection: ApiConectionProvider) {
    console.log('Hello UserProvider Provider');
  }
  getDataUser() {
    const promise = new Promise((resolve, reject) => {
      this._apiConnection.get('dataUser')
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }
    registerStudent(params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.post('studentRegister', params)
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                    },
                    msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }
    changePassgord(params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.put('changePassword', params)
                .toPromise()
                .then(
                    res => {
                        resolve(res);
                    },
                    msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }
    resetPassword(params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.put('reset2', params)
                .toPromise()
                .then(
                    res => { // Success
                        // console.log(res);
                        resolve(res);
                    },
                    msg => {
                        // console.log(msg);
                        reject(msg);
                    }
                );
        });
        return promise;
    }

    updateUser(params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.put('studentEdit', params)
                .toPromise()
                .then(res => {
                        resolve(res);
                    },
                    msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }

    logout() {
      localStorage.clear();
      localStorage.removeItem('token');

  }

    studentUpdate(id, params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConnection.put('studentUpdate/' + id, params)
                .toPromise()
                .then(res => {
                        resolve(res);
                    },
                    msg => {
                        reject(msg);
                    }
                );
        });
        return promise;
    }



}
