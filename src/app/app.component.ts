import { Component } from '@angular/core';
import { App, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from "../pages/login/login";
import { FCM } from '@ionic-native/fcm';
import {AlertController} from "ionic-angular";

import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  token: String;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public menuCtrl: MenuController, public app: App, private fcm: FCM, public alerta: AlertController) {

    this.token = window.localStorage.getItem('token');

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      if (platform.is('cordova')){

          this.fcm.subscribeToTopic('all');
          this.fcm.getToken().then(token => {
              // Envias userId y el token al backend
              // para registrarlo (1 usuario tiene muchos tokens)
              console.log("Device token:" + token);
              // this.alerta.create({
              //     title: 'ID Dispositivo1',
              //     message: token
              // }).present();

              // Subirlo al backend... y almacenarlo
              // Convien
          });

          this.fcm.onNotification().subscribe(data => {
              this.alerta.create({
                  title: 'Nuevo Evento',
                  message: JSON.stringify(data.a_data),
                  buttons:['OK']
              }).present();
              //console.log(JSON.stringify(data));
              if(data.wasTapped){
                  console.log("Received in background");
                  console.log(JSON.stringify(data));
              } else {
                  console.log("Received in foreground");
                  console.log(JSON.stringify(data));
              };
          });

          this.fcm.onTokenRefresh().subscribe(token=>{
              console.log(token);
          });
      }


      console.log(this.token);
      if(this.token !== null && this.token !== undefined){
        this.rootPage = TabsPage;
      }else{
        this.menuCtrl.swipeEnable(false);
        this.app.getRootNav().setRoot(LoginPage);
      }




    });
  }
  cerrarSession() {
    window.localStorage.removeItem('token');
    window.localStorage.clear();
    this.rootPage = LoginPage;
  }
}
