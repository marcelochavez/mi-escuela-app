import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from "../pages/login/login";
import {EventosPage} from "../pages/eventos/eventos";
import {CursosPage} from "../pages/cursos/cursos";
import {PerfilPage} from "../pages/perfil/perfil";
import {RegisterPage} from "../pages/register/register";
import {DetalleEventoPage} from "../pages/detalle-evento/detalle-evento";
import {CambiarPasswordPage} from "../pages/cambiar-password/cambiar-password";
import {ResetPasswordPage} from "../pages/reset-password/reset-password";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiConectionProvider } from '../providers/api-conection/api-conection';
import { LoginProvider } from '../providers/login/login';
import { UserProvider } from '../providers/user/user';
import { CourseProvider } from '../providers/course/course';
import { EventProvider } from '../providers/event/event';
import { FCM } from '@ionic-native/fcm';



@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    EventosPage,
    CursosPage,
    PerfilPage,
      DetalleEventoPage,
      RegisterPage,
      CambiarPasswordPage,
      ResetPasswordPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    EventosPage,
    CursosPage,
    PerfilPage,
      DetalleEventoPage,
      RegisterPage,
      CambiarPasswordPage,
      ResetPasswordPage
  ],
  providers: [
    FCM,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiConectionProvider,
    LoginProvider,
    UserProvider,
    CourseProvider,
    EventProvider
  ]
})
export class AppModule {}
