export class AuthenticationModel {

  public email: String;
  public password: String;

  constructor() {
  }
}
export class ChangePassModel {
  public password: String;
  public newPassword: String;
}
