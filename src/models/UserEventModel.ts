import {dataUserModel} from "./dataUser";
import {EventModel} from "./event";


export class UserEventModel {
    public id: number;
    public student_id: number;
    public event_id: number;
    public description: String;
    public created_at: Date;
    public event: EventModel;
    public user: dataUserModel;
    constructor() {}
}

