export class CourseInstanceModel {
  public id: number;
  public course_id: number;
  public semester_id: number;
  public year_id: number;
  public course: CourseModel;
  public semester: SemesterModel;
  public year: YearModel;
  constructor() {}
}


export class CourseModel {
  public id: number;
  public code: String;
  public name: String;
  public credits: number;
  public description: String;
  public type_course_id: number;
  public type_course: TypeCourseModel;
  constructor() {
  }
}

export class SemesterModel {
  public id: number;
  public name: String;
  constructor() {
  }
}

export class YearModel {
  public id: number;
  public name: String;
}

export class TypeCourseModel {
  public id: number;
  public name: String;
}
