import {CourseInstanceEventModel} from './CourseInstanceEventModel';
import {GlobalEventModel} from './GlobalEventModel';
import {UserEventModel} from './UserEventModel';

export class EventModel {
    public id: number;
    public priority_id: number;
    public event_type_id: number;
    public event_date: Date;
    public created_at: Date;
    public priority: PriorityModel;
    public eventoPara: String;
    public events_type: EventTypeModel;
    public course_instance_has_events: CourseInstanceEventModel;
    public global_events: GlobalEventModel;
    public users_has_events: UserEventModel[];


    constructor() {}
}

export class PriorityModel {
    public id: number;
    public name: String;
    constructor() {
    }
}

export class EventTypeModel {
    public id: number;
    public name: String;
    public category_id: number;
    public category: CategoryModel;
    constructor() {
    }
}

export class CategoryModel {
    public id: number;
    public name: String;
}

