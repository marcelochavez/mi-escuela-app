export class UserRegisterModel {

  public name: String;
  public last_name: String;
  public email: String;
  public phone: String;

  constructor() {
  }
}
