import {EventModel} from "./event";


export class GlobalEventModel {
    public id: number;
    public event_id: number;
    public description: String;
    public created_at: Date;
    public event: EventModel;
    constructor() {}
}
