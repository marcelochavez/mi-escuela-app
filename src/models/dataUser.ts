export class dataUserModel {
    public id: number;
    public name: String;
    public last_name: String;
    public email: string;
    public phone: String;
    public role_id: String;
    public reset_finish_date: String;
    public courseCount: number;
    public student_has_course_instances: StudentHasCourseInstances;

    constructor() {
    }
}

export class StudentHasCourseInstances {
    public id: number;
    public student_id: number;
    public course_instance_id: number;
    public course_instance: CourseIntance;

    constructor() {
    }
}

export  class CourseIntance {
    public id: number;
    public course_id: number;
    public semester_id: number;
    public years_id: number;
    public course: Course;
    public semester: Semester;
    public year: Year;
    constructor() { }
}

export class Course {
    public id: number;
    public code: string;
    public name: string;
    public credits: number;
    public description: string;
    public type_course_id: number;
    constructor() { }
}

export class Semester {
    public id: number;
    public name: string;
    constructor() {
    }
}

export class Year {
    public id: number;
    public name: string;
    constructor() { }
}

