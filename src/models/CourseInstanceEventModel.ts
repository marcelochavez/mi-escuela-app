import {EventModel} from "./event";
import {CourseInstanceModel} from "./course";

export class CourseInstanceEventModel {
    public id: number;
    public course_instance_id: number;
    public event_id: number;
    public description: String;
    public created_at: Date;
    public course_instance: CourseInstanceModel;
    public event: EventModel;

    constructor() {}
}
