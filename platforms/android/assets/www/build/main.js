webpackJsonp([8],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CambiarPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_authentication__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CambiarPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CambiarPasswordPage = /** @class */ (function () {
    function CambiarPasswordPage(navCtrl, navParams, _providerUser, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerUser = _providerUser;
        this.loadingCtrl = loadingCtrl;
        this.newPass = new __WEBPACK_IMPORTED_MODULE_2__models_authentication__["b" /* ChangePassModel */]();
    }
    CambiarPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CambiarPasswordPage');
    };
    CambiarPasswordPage.prototype.cambiarPass = function () {
        var _this = this;
        this._providerUser.changePassgord(this.newPass).then(function (value) {
            _this.ionViewLoadedPassword();
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
            //       this.navCtrl.push(EventosPage);
        }, function (error) {
        });
    };
    CambiarPasswordPage.prototype.ionViewLoadedPassword = function () {
        var loading = this.loadingCtrl.create({
            content: 'Actualizando Contraseña'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 500);
    };
    CambiarPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cambiar-password',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/cambiar-password/cambiar-password.html"*/'<ion-header>\n  <ion-navbar color="tabs">\n    <ion-title>\n      Cambiar Constraseña\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header></ion-list-header>\n  </ion-list>\n\n  <ion-item>\n    <ion-label>Contraseña Actual</ion-label>\n    <ion-input type="password"  [(ngModel)]="newPass.password" name="name"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Nueva Contraseña</ion-label>\n    <ion-input type="password"  [(ngModel)]="newPass.newPassword" name="last_name"></ion-input>\n  </ion-item>\n\n  <div>\n    <button ion-button item-start (click)="cambiarPass()">Cambiar Contraseña</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/cambiar-password/cambiar-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], CambiarPasswordPage);
    return CambiarPasswordPage;
}());

//# sourceMappingURL=cambiar-password.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_event_event__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detalle_evento_detalle_evento__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventosPage = /** @class */ (function () {
    function EventosPage(navCtrl, navParams, _providerEvent, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerEvent = _providerEvent;
        this.loadingCtrl = loadingCtrl;
        this.allEvents = [];
        this.globalEvents = [];
        this.courseInstanceEvents = [];
        this.userEvents = [];
    }
    EventosPage.prototype.ionViewLoaded = function () {
        var loading = this.loadingCtrl.create({
            content: 'Cargando Eventos'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 2000);
    };
    EventosPage.prototype.ionViewWillEnter = function () {
        this.ionViewLoaded();
        this.cargarAllEvent();
        this.cargarGlobalEvent();
        this.cargarCourseInstanceEvent();
        this.cargarUserEvent();
    };
    EventosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventosPage');
    };
    EventosPage.prototype.cargarGlobalEvent = function () {
        var _this = this;
        this._providerEvent.getGlobalEvent().then(function (value) {
            _this.globalEvents = value['data'];
            console.log(value);
        }, function (error) {
        });
    };
    EventosPage.prototype.cargarCourseInstanceEvent = function () {
        var _this = this;
        this._providerEvent.getCourseInstanceEvent().then(function (value) {
            _this.courseInstanceEvents = value['data'];
            console.log(value);
        }, function (error) {
        });
    };
    EventosPage.prototype.cargarUserEvent = function () {
        var _this = this;
        this._providerEvent.getUserEvent().then(function (value) {
            _this.userEvents = value['data'];
            console.log(value);
        }, function (error) {
        });
    };
    EventosPage.prototype.cargarAllEvent = function () {
        var _this = this;
        this._providerEvent.getAllEvent().then(function (value) {
            _this.allEvents = value['data'];
            console.log(value);
        }, function (error) {
        });
    };
    EventosPage.prototype.irDetalleEvento = function (event) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detalle_evento_detalle_evento__["a" /* DetalleEventoPage */], { event: event });
    };
    EventosPage.prototype.doRefresh = function (refresher) {
        this.cargarAllEvent();
        this.cargarGlobalEvent();
        this.cargarCourseInstanceEvent();
        this.cargarUserEvent();
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 700);
    };
    EventosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eventos',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/eventos/eventos.html"*/'<ion-header>\n  <ion-navbar color="tabs">\n    <ion-title>Eventos</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n\n    <ion-content padding>\n        <ion-refresher (ionRefresh)= "doRefresh($event)">\n            <ion-refresher-content refreshingText="Actualizando..."></ion-refresher-content>\n        </ion-refresher>\n        <ion-card  *ngFor="let event of allEvents">\n            <button ion-item (click)="irDetalleEvento(event)">\n                <ion-card-content>\n                    <ion-card-title>\n                        {{event.events_type.name}}\n                    </ion-card-title>\n                    <p>\n                        <ion-item *ngIf="event.eventoPara == \'Evento de Curso\'">\n                            <strong>{{event.course_instance_has_events[0].course_instance.course.code}} {{event.course_instance_has_events[0].course_instance.course.name}}</strong>\n                        </ion-item>\n\n                        <ion-item *ngIf="event.eventoPara == \'Evento de Estudiante\'">\n                            <strong>{{this.event.users_has_events[0].description}}</strong>\n                        </ion-item>\n\n\n                        Publicado por: {{event.events_type.category.name}} <br>\n                        Envio: {{event.created_at}} <br> {{event.priority.name}}\n\n                    </p>\n                </ion-card-content>\n            </button>\n        </ion-card>\n    </ion-content>\n\n\n\n\n\n\n\n\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/eventos/eventos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_event_event__["a" /* EventProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], EventosPage);
    return EventosPage;
}());

//# sourceMappingURL=eventos.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalleEventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DetalleEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetalleEventoPage = /** @class */ (function () {
    function DetalleEventoPage(navCtrl, navParams, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.event = navParams.data.event;
        this.obtenerDescripcion();
        console.log(this.event);
    }
    DetalleEventoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetalleEventoPage');
    };
    DetalleEventoPage.prototype.obtenerDescripcion = function () {
        if (this.event.eventoPara == 'Evento Global') {
            this.description = this.event.global_events[0].description;
        }
        else if (this.event.eventoPara == 'Evento de Estudiante') {
            this.description = this.event.users_has_events[0].description;
        }
        else if (this.event.eventoPara == 'Evento de Curso') {
            this.description = this.event.course_instance_has_events[0].description;
        }
    };
    DetalleEventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalle-evento',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/detalle-evento/detalle-evento.html"*/'<!--\n  Generated template for the DetalleEventoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="tabs">\n    <ion-title>Detalle De Evento</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-card>\n      <ion-card-content>\n        <ion-card-title>\n          <ion-label>{{event.events_type.name}}</ion-label>\n\n        </ion-card-title>\n          <ion-item *ngIf="event.eventoPara == \'Evento de Curso\'">\n              {{event.course_instance_has_events[0].course_instance.course.code}} {{event.course_instance_has_events[0].course_instance.course.name}}\n          </ion-item>\n          <ion-label>Fecha de Evento: {{event.event_date}}</ion-label>\n          <ion-label>Evento Publicado Por: {{event.events_type.category.name}}</ion-label>\n          <ion-label>Fecha publicacion: {{event.created_at}}</ion-label>\n\n          <p>\n            <strong>{{description}}</strong>\n\n\n\n          </p>\n      </ion-card-content>\n  </ion-card>\n</ion-content>\n\n\n\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/detalle-evento/detalle-evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], DetalleEventoPage);
    return DetalleEventoPage;
}());

//# sourceMappingURL=detalle-evento.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CursosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_course_course__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_dataUser__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CursosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CursosPage = /** @class */ (function () {
    function CursosPage(navCtrl, navParams, _providerCourse, _providerUser, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerCourse = _providerCourse;
        this._providerUser = _providerUser;
        this.loadingCtrl = loadingCtrl;
        this.courses = [];
        this.coursesStudent = [];
        this.courseInstanceStudent = [];
        this.allCourse = [];
        this.cursosItem = [];
        this.userData = new __WEBPACK_IMPORTED_MODULE_4__models_dataUser__["a" /* dataUserModel */]();
    }
    CursosPage_1 = CursosPage;
    CursosPage.prototype.ionViewWillEnter = function () {
        this.ionViewLoaded();
        this.obtenerInstanciasCursosActuales();
        this.obtenerInstanciasCursosEstudiante();
        this.obtenerUsuario();
    };
    CursosPage.prototype.ionViewDidLoad = function () {
        this.obtenerInstanciasCursosActuales();
        this.obtenerInstanciasCursosEstudiante();
        this.obtenerUsuario();
        console.log('ionViewDidLoad CursosPage');
    };
    CursosPage.prototype.ionViewLoaded = function () {
        var loading = this.loadingCtrl.create({
            content: 'Cargando Cursos'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 500);
    };
    CursosPage.prototype.obtenerInstanciasCursosActuales = function () {
        var _this = this;
        this._providerCourse.getListInstanceCourse().then(function (value) {
            _this.courses = value['data'];
            console.log(_this.courses);
        }, function (error) {
        });
    };
    CursosPage.prototype.obtenerInstanciasCursosEstudiante = function () {
        var _this = this;
        this._providerCourse.getIntanceCourseByStudent().then(function (value) {
            _this.courseInstanceStudent = value['data'];
            _this.pasarACurso();
        }, function (error) {
        });
    };
    CursosPage.prototype.pasarACurso = function () {
        var i;
        i = 0;
        while (i < this.courseInstanceStudent.length) {
            this.coursesStudent[i] = this.courseInstanceStudent[i].course;
            //this.courseSelect[i] = this.courseInstanceStudent[i].course;
            this.allCourse[i] = this.courseInstanceStudent[i].course_id;
            i++;
        }
        this.pasarAItem();
    };
    CursosPage.prototype.pasarAItem = function () {
        var i, j, selected;
        i = 0;
        this.cursosItem = [{
                name: String('Seleccione Curso'), value: null, selected: false, disabled: true
            }];
        while (i < this.courses.length) {
            j = 0;
            selected = false;
            while (j < this.allCourse.length) {
                if (this.courses[i].id == this.allCourse[j]) {
                    selected = true;
                }
                j++;
            }
            this.cursosItem.push({
                name: String(this.courses[i].code + ' ' + this.courses[i].name),
                value: this.courses[i].id,
                selected: selected,
                disabled: false
            });
            i++;
        }
        console.log(this.cursosItem);
    };
    CursosPage.prototype.actualizar = function (event, idCurso) {
        if (event.checked) {
            this.allCourse.push(idCurso);
            //agregar
        }
        else {
            var i = void 0;
            i = 0;
            while (i < this.allCourse.length) {
                if (this.allCourse[i] == idCurso) {
                    this.allCourse.splice(i, 1);
                }
                i++;
            }
        }
        console.log(this.allCourse);
    };
    CursosPage.prototype.guardar = function () {
        var _this = this;
        var paramStudent = {
            'name': this.userData.name,
            'last_name': this.userData.last_name,
            'email': this.userData.email,
            'phone': this.userData.phone,
            'course_id': this.allCourse
        };
        console.log(paramStudent);
        this._providerUser.studentUpdate(this.userData.id, paramStudent).then(function (res) {
            _this.navCtrl.push(CursosPage_1);
        }, function (error) {
            console.log(error);
        });
    };
    CursosPage.prototype.obtenerUsuario = function () {
        var _this = this;
        this._providerUser.getDataUser().then(function (value) {
            _this.userData = value['data'];
            console.log(_this.userData);
        }, function (error) {
            console.log(error);
        });
    };
    CursosPage.prototype.doRefresh = function (refresher) {
        this.obtenerInstanciasCursosActuales();
        this.obtenerInstanciasCursosEstudiante();
        this.obtenerUsuario();
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 500);
    };
    CursosPage = CursosPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cursos',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/cursos/cursos.html"*/'<ion-header>\n  <ion-navbar color="tabs">\n    <ion-title>\n      Cursos\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)= "doRefresh($event)">\n    <ion-refresher-content refreshingText="Actualizando..."></ion-refresher-content>\n  </ion-refresher>\n\n<form>\n  <ion-list-header>Mis Cursos</ion-list-header>\n  <button ion-button item-start (click)="guardar()">Actualizar Cursos</button>\n\n  <ion-item *ngFor="let courseStudent of cursosItem" >\n    <ion-label>{{courseStudent.name}}</ion-label>\n      <ion-checkbox (ionChange)="actualizar($event, courseStudent.value)" checked={{courseStudent.selected}} disabled="{{courseStudent.disabled}}"></ion-checkbox>\n  </ion-item>\n</form>\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/cursos/cursos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_course_course__["a" /* CourseProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], CursosPage);
    return CursosPage;
    var CursosPage_1;
}());

//# sourceMappingURL=cursos.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_dataUser__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, _providerUser, alerta, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerUser = _providerUser;
        this.alerta = alerta;
        this.loadingCtrl = loadingCtrl;
        this.userData = new __WEBPACK_IMPORTED_MODULE_2__models_dataUser__["a" /* dataUserModel */]();
    }
    PerfilPage.prototype.ionViewWillEnter = function () {
        this.ionViewLoaded();
        this.obtenerUsuario();
    };
    PerfilPage.prototype.ionViewDidLoad = function () {
        this.ionViewLoaded();
        this.obtenerUsuario();
        console.log('ionViewDidLoad PerfilPage');
    };
    PerfilPage.prototype.ionViewLoaded = function () {
        var loading = this.loadingCtrl.create({
            content: 'Cargando Perfil'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
    };
    PerfilPage.prototype.obtenerUsuario = function () {
        var _this = this;
        this._providerUser.getDataUser().then(function (value) {
            _this.userData = value['data'];
            console.log(_this.userData);
        }, function (error) {
            console.log(error);
        });
    };
    PerfilPage.prototype.editarPerfil = function () {
        var _this = this;
        this._providerUser.updateUser(this.userData).then(function (res) {
            var alert = _this.alerta.create({
                title: 'Éxito',
                message: 'Se Registraron los Cambios de Forma Correcta',
                buttons: ['Aceptar']
            });
            alert.present();
        }, function (error) {
            var errorAlert = _this.alerta.create({
                title: 'Error al Editar',
                message: 'Favor, Verifique la información',
                buttons: ['Aceptar']
            });
            errorAlert.present();
        });
        console.log(this.userData);
    };
    PerfilPage.prototype.cerrarSesion = function () {
        this.ionViewLoadedCerrarSesion();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
        this._providerUser.logout();
        // this.navCtrl.push(LoginPage);
    };
    PerfilPage.prototype.ionViewLoadedCerrarSesion = function () {
        var loading = this.loadingCtrl.create({
            content: 'Cerrando sesión'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 500);
    };
    PerfilPage.prototype.doRefresh = function (refresher) {
        this.obtenerUsuario();
        this.obtenerUsuario();
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 500);
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/perfil/perfil.html"*/'<ion-header>\n  <ion-navbar color="tabs">\n    <ion-title>\n      Mis Datos\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)= "doRefresh($event)">\n    <ion-refresher-content refreshingText="Actualizando..."></ion-refresher-content>\n  </ion-refresher>\n  <ion-list>\n    <ion-list-header>Datos de Estudiante</ion-list-header>\n  </ion-list>\n\n  <form >\n    <ion-item>\n      <ion-label>Nombre</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.name" name="name"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Apellido</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.last_name" name="last_name"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Email</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.email" name="email"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Teléfono</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.phone" name="phone"></ion-input>\n    </ion-item>\n    <div>\n      <button ion-button (click)="editarPerfil()">Editar</button>\n    </div>\n  </form>\n\n  <button ion-button (click)="cerrarSesion()">Cerrar Sesión</button>\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/perfil/perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_userRegister__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, _providerUser, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerUser = _providerUser;
        this.loadingCtrl = loadingCtrl;
        this.userData = new __WEBPACK_IMPORTED_MODULE_2__models_userRegister__["a" /* UserRegisterModel */]();
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.registerForm = function () {
        var _this = this;
        console.log(this.userData);
        this._providerUser.registerStudent(this.userData).then(function (value) {
            _this.ionViewLoadedRegistrar();
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
        }, function (error) {
        });
    };
    RegisterPage.prototype.ionViewLoadedRegistrar = function () {
        var loading = this.loadingCtrl.create({
            content: 'Registrando Usuario'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 3000);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/register/register.html"*/'<ion-header>\n  <ion-navbar color="tabs">\n    <ion-title>\n      Registrar\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Datos de Estudiante</ion-list-header>\n  </ion-list>\n\n    <ion-item>\n      <ion-label>Nombre</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.name" name="name"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Apellido</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.last_name" name="last_name"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Email</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.email" name="email"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Teléfono</ion-label>\n      <ion-input type="text" [(ngModel)]="userData.phone" name="phone"></ion-input>\n    </ion-item>\n    <div>\n      <button ion-button item-start (click)="registerForm()">Registrar</button>\n    </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResetPasswordPage = /** @class */ (function () {
    function ResetPasswordPage(navCtrl, navParams, _providerUser, alerta, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerUser = _providerUser;
        this.alerta = alerta;
        this.loadingCtrl = loadingCtrl;
    }
    ResetPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetPasswordPage');
    };
    ResetPasswordPage.prototype.ionViewLoaded = function () {
        var loading = this.loadingCtrl.create({
            content: 'Actualizando Contraseña'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
    };
    ResetPasswordPage.prototype.restaurarPass = function (email) {
        var _this = this;
        var param = {
            'email': email
        };
        console.log(param);
        this._providerUser.resetPassword(param).then(function (res) {
            var alert = _this.alerta.create({
                title: 'Restauración de Contraseña',
                message: 'Se ha enviado un contraseña a su Correo Electrónico',
                buttons: ['Aceptar']
            });
            alert.present();
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        }, function (error) {
            var errorAlert = _this.alerta.create({
                title: 'Error al restaurar Contraseña',
                message: 'Favor, Verifique que el Correo sea el correcto',
                buttons: ['Aceptar']
            });
            errorAlert.present();
        });
    };
    ResetPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reset-password',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/reset-password/reset-password.html"*/'<!--\n  Generated template for the ResetPasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="tabs">\n    <ion-title>Restaurar Contraseña</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-item>\n    <ion-input placeholder="Ingrese su Correo" type="text" [(ngModel)]="email"></ion-input>\n  </ion-item>\n  <br>\n  <br>\n  <button ion-button align-items-center (click)="restaurarPass(email)">Restaurar Contraseña</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/reset-password/reset-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], ResetPasswordPage);
    return ResetPasswordPage;
}());

//# sourceMappingURL=reset-password.js.map

/***/ }),

/***/ 123:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 123;

/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cambiar-password/cambiar-password.module": [
		294,
		7
	],
	"../pages/cursos/cursos.module": [
		295,
		6
	],
	"../pages/detalle-evento/detalle-evento.module": [
		296,
		5
	],
	"../pages/eventos/eventos.module": [
		297,
		4
	],
	"../pages/login/login.module": [
		298,
		3
	],
	"../pages/perfil/perfil.module": [
		299,
		2
	],
	"../pages/register/register.module": [
		301,
		1
	],
	"../pages/reset-password/reset-password.module": [
		300,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 165;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ChangePassModel; });
var AuthenticationModel = /** @class */ (function () {
    function AuthenticationModel() {
    }
    return AuthenticationModel;
}());

var ChangePassModel = /** @class */ (function () {
    function ChangePassModel() {
    }
    return ChangePassModel;
}());

//# sourceMappingURL=authentication.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_conection_api_conection__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the EventProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var EventProvider = /** @class */ (function () {
    function EventProvider(_apiConnection) {
        this._apiConnection = _apiConnection;
        console.log('Hello EventProvider Provider');
    }
    EventProvider.prototype.getAllEvent = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('getStudentEvents')
                .toPromise()
                .then(function (res) {
                resolve(res);
                console.log(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    EventProvider.prototype.getCourseInstanceEvent = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('courseInstanceHasEvents')
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    EventProvider.prototype.getGlobalEvent = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('globalEvents')
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    EventProvider.prototype.getUserEvent = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('usersHasEvents')
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    EventProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__api_conection_api_conection__["a" /* ApiConectionProvider */]])
    ], EventProvider);
    return EventProvider;
}());

//# sourceMappingURL=event.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CourseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_conection_api_conection__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the CourseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CourseProvider = /** @class */ (function () {
    function CourseProvider(_apiConnection) {
        this._apiConnection = _apiConnection;
        console.log('Hello CourseProvider Provider');
    }
    // Obtener cursos que perteneces a la instancia acutual
    CourseProvider.prototype.getListInstanceCourse = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('currentInstancesCourses')
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    CourseProvider.prototype.getIntanceCourseByStudent = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('getIntanceCourseByStudent')
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    CourseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_conection_api_conection__["a" /* ApiConectionProvider */]])
    ], CourseProvider);
    return CourseProvider;
}());

//# sourceMappingURL=course.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dataUserModel; });
/* unused harmony export StudentHasCourseInstances */
/* unused harmony export CourseIntance */
/* unused harmony export Course */
/* unused harmony export Semester */
/* unused harmony export Year */
var dataUserModel = /** @class */ (function () {
    function dataUserModel() {
    }
    return dataUserModel;
}());

var StudentHasCourseInstances = /** @class */ (function () {
    function StudentHasCourseInstances() {
    }
    return StudentHasCourseInstances;
}());

var CourseIntance = /** @class */ (function () {
    function CourseIntance() {
    }
    return CourseIntance;
}());

var Course = /** @class */ (function () {
    function Course() {
    }
    return Course;
}());

var Semester = /** @class */ (function () {
    function Semester() {
    }
    return Semester;
}());

var Year = /** @class */ (function () {
    function Year() {
    }
    return Year;
}());

//# sourceMappingURL=dataUser.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_conection_api_conection__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';


/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LoginProvider = /** @class */ (function () {
    function LoginProvider(_apiConnection) {
        this._apiConnection = _apiConnection;
        console.log('Hello LoginProvider Provider');
    }
    LoginProvider.prototype.loginUser = function (userInfo) {
        var _this = this;
        localStorage.clear();
        var promise = new Promise(function (resolve, reject) {
            console.log(userInfo);
            _this._apiConnection.post('auth/studentLogin', userInfo)
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    LoginProvider.prototype.registerUserDevice = function (params) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.post('usersHasDevices', params)
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_conection_api_conection__["a" /* ApiConectionProvider */]])
    ], LoginProvider);
    return LoginProvider;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(235);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_eventos_eventos__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_cursos_cursos__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_perfil_perfil__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_register_register__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_detalle_evento_detalle_evento__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_cambiar_password_cambiar_password__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_reset_password_reset_password__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_status_bar__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_splash_screen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_api_conection_api_conection__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_login_login__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_user_user__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_course_course__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_event_event__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_fcm__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_eventos_eventos__["a" /* EventosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cursos_cursos__["a" /* CursosPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_detalle_evento_detalle_evento__["a" /* DetalleEventoPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_cambiar_password_cambiar_password__["a" /* CambiarPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_reset_password_reset_password__["a" /* ResetPasswordPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/cambiar-password/cambiar-password.module#CambiarPasswordPageModule', name: 'CambiarPasswordPage', segment: 'cambiar-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cursos/cursos.module#CursosPageModule', name: 'CursosPage', segment: 'cursos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detalle-evento/detalle-evento.module#DetalleEventoPageModule', name: 'DetalleEventoPage', segment: 'detalle-evento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eventos/eventos.module#EventosPageModule', name: 'EventosPage', segment: 'eventos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reset-password/reset-password.module#ResetPasswordPageModule', name: 'ResetPasswordPage', segment: 'reset-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_eventos_eventos__["a" /* EventosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cursos_cursos__["a" /* CursosPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_detalle_evento_detalle_evento__["a" /* DetalleEventoPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_cambiar_password_cambiar_password__["a" /* CambiarPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_reset_password_reset_password__["a" /* ResetPasswordPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_fcm__["a" /* FCM */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_api_conection_api_conection__["a" /* ApiConectionProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_login_login__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_22__providers_course_course__["a" /* CourseProvider */],
                __WEBPACK_IMPORTED_MODULE_23__providers_event_event__["a" /* EventProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRegisterModel; });
var UserRegisterModel = /** @class */ (function () {
    function UserRegisterModel() {
    }
    return UserRegisterModel;
}());

//# sourceMappingURL=userRegister.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_fcm__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl, app, fcm, alerta) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.app = app;
        this.fcm = fcm;
        this.alerta = alerta;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */];
        this.token = window.localStorage.getItem('token');
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            if (platform.is('cordova')) {
                _this.fcm.subscribeToTopic('all');
                _this.fcm.getToken().then(function (token) {
                    // Envias userId y el token al backend
                    // para registrarlo (1 usuario tiene muchos tokens)
                    console.log("Device token:" + token);
                    // this.alerta.create({
                    //     title: 'ID Dispositivo1',
                    //     message: token
                    // }).present();
                    // Subirlo al backend... y almacenarlo
                    // Convien
                });
                _this.fcm.onNotification().subscribe(function (data) {
                    _this.alerta.create({
                        title: 'Nuevo Evento',
                        message: JSON.stringify(data.a_data),
                        buttons: ['OK']
                    }).present();
                    //console.log(JSON.stringify(data));
                    if (data.wasTapped) {
                        console.log("Received in background");
                        console.log(JSON.stringify(data));
                    }
                    else {
                        console.log("Received in foreground");
                        console.log(JSON.stringify(data));
                    }
                    ;
                });
                _this.fcm.onTokenRefresh().subscribe(function (token) {
                    console.log(token);
                });
            }
            console.log(_this.token);
            if (_this.token !== null && _this.token !== undefined) {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */];
            }
            else {
                _this.menuCtrl.swipeEnable(false);
                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
            }
        });
    }
    MyApp.prototype.cerrarSession = function () {
        window.localStorage.removeItem('token');
        window.localStorage.clear();
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_fcm__["a" /* FCM */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Mis Datos\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-start></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h2>Welcome to Ionic!</h2>\n  <p>\n    This starter project comes with simple tabs-based layout for apps\n    that are going to primarily use a Tabbed UI.\n  </p>\n  <p>\n    Take a look at the <code>src/pages/</code> directory to add or change tabs,\n    update any existing page or create new pages.\n  </p>\n</ion-content>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_conection_api_conection__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UserProvider = /** @class */ (function () {
    function UserProvider(_apiConnection) {
        this._apiConnection = _apiConnection;
        console.log('Hello UserProvider Provider');
    }
    UserProvider.prototype.getDataUser = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.get('dataUser')
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    UserProvider.prototype.registerStudent = function (params) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.post('studentRegister', params)
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    UserProvider.prototype.changePassgord = function (params) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.put('changePassword', params)
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    UserProvider.prototype.resetPassword = function (params) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.put('reset2', params)
                .toPromise()
                .then(function (res) {
                // console.log(res);
                resolve(res);
            }, function (msg) {
                // console.log(msg);
                reject(msg);
            });
        });
        return promise;
    };
    UserProvider.prototype.updateUser = function (params) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.put('studentEdit', params)
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    UserProvider.prototype.logout = function () {
        localStorage.clear();
        localStorage.removeItem('token');
    };
    UserProvider.prototype.studentUpdate = function (id, params) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this._apiConnection.put('studentUpdate/' + id, params)
                .toPromise()
                .then(function (res) {
                resolve(res);
            }, function (msg) {
                reject(msg);
            });
        });
        return promise;
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_conection_api_conection__["a" /* ApiConectionProvider */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_authentication__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_login_login__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cambiar_password_cambiar_password__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__reset_password_reset_password__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_fcm__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, _loginService, alerta, fcm, platform, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._loginService = _loginService;
        this.alerta = alerta;
        this.fcm = fcm;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.userAuth = new __WEBPACK_IMPORTED_MODULE_2__models_authentication__["a" /* AuthenticationModel */]();
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        localStorage.clear();
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this._loginService.loginUser(this.userAuth).then(function (value) {
            console.log(value['data'].token);
            window.localStorage.setItem('token', value['data'].token);
            if (value['data'].changePass) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__cambiar_password_cambiar_password__["a" /* CambiarPasswordPage */]);
            }
            else {
                if (_this.platform.is('android')) {
                    _this.fcm.getToken().then(function (token) {
                        var params = {
                            'device': token,
                            'users_id': value['data'].id
                        };
                        console.log('PARAMETROS:' + params);
                        _this._loginService.registerUserDevice(params).then(function (value) {
                            //  console.log('PARAMETROS:'+params);
                        }, function (error) {
                            //  console.log("Error")
                        });
                        // Enviar usuario y token al back
                        // this._loginService.registerUserDevice(userId, token).then(_ => {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
                        //});
                    });
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
                }
            }
        }, function (error) {
            var errorAlert = _this.alerta.create({
                title: 'No se pudo Iniciar Sesión',
                message: 'Contraseña o Correo Incorrectos',
                buttons: ['Cerrar']
            });
            errorAlert.present();
            console.log(error);
        });
    };
    LoginPage.prototype.irRegistrar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.restaurarPass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__reset_password_reset_password__["a" /* ResetPasswordPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="tabs">\n\n    <ion-title>Ingresar</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n  <ion-content padding justify-content-end="">\n\n    <ion-grid justify-content-center>\n            <ion-list >\n\n              <ion-item>\n                <ion-input placeholder="Correo" type="text" [(ngModel)]="userAuth.email"></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-input placeholder="Contraseña" type="password" [(ngModel)]="userAuth.password"></ion-input>\n              </ion-item>\n            </ion-list>\n\n      <a text-center (click)="restaurarPass()">¿Olvidaste tu Contraseña?</a>\n\n      <div ion-row>\n        <div col-6>\n          <button ion-button item-start (click)="login()">Ingresar</button>\n        </div>\n        <div col-6>\n          <button ion-button item-end (click)="irRegistrar()">Registrar</button>\n        </div>\n      </div>\n\n    </ion-grid>\n  </ion-content>\n\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_login_login__["a" /* LoginProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_fcm__["a" /* FCM */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiConectionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ApiConectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ApiConectionProvider = /** @class */ (function () {
    function ApiConectionProvider(http) {
        this.http = http;
        //this.url = 'http://localhost:8000/api';
        // this.url = 'http://10.0.2.2:8000/api';
        //this.url = 'http://158.251.255.225:8000/api';
        this.url = 'http://192.168.0.15:8000/api';
        // this.url = 'http://10.8.227.2:8000/api';
    }
    ApiConectionProvider.prototype.createAuthorizationHeader = function (headers) {
        this.idToken = window.localStorage.getItem("token");
        console.log("the idtoken", this.idToken);
        headers = headers.set('Authorization', 'Bearer ' + this.idToken);
        return headers;
    };
    ApiConectionProvider.prototype.get = function (endpoint, params, reqOpts) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({});
        headers = this.createAuthorizationHeader(headers);
        // console.log(headers.get('Authorization'));
        if (!reqOpts) {
            reqOpts = {
                params: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */](),
                headers: headers
            };
        }
        // else{
        //   // console.log("entro else api");
        //   reqOpts = {
        //     headers: headers
        //   }
        // }
        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
            for (var k in params) {
                reqOpts.params.set(k, params[k]);
            }
        }
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    };
    ApiConectionProvider.prototype.post = function (endpoint, body, reqOpts) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    };
    ApiConectionProvider.prototype.put = function (endpoint, body, reqOpts) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    };
    ApiConectionProvider.prototype.delete = function (endpoint, reqOpts) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    };
    ApiConectionProvider.prototype.patch = function (endpoint, body, reqOpts) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    };
    ApiConectionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ApiConectionProvider);
    return ApiConectionProvider;
}());

//# sourceMappingURL=api-conection.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__eventos_eventos__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cursos_cursos__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__perfil_perfil__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__eventos_eventos__["a" /* EventosPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__cursos_cursos__["a" /* CursosPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__perfil_perfil__["a" /* PerfilPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/tabs/tabs.html"*/'<ion-tabs color="tabs">\n  <ion-tab [root]="tab1Root" tabTitle="Eventos" tabIcon="md-information-circle"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Curso" tabIcon="md-school"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Perfil" tabIcon="md-person"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/mchav/Desktop/Mi-escuela/mi-escuela-app/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ })

},[214]);
//# sourceMappingURL=main.js.map